const initialState = []
const dataReducer = (state=initialState, action) => {
    console.log("in datareducer")
    switch(action.type){
      case "FETCH-DATA":
        const newState = initialState
        for (var i = 0; i < action.payload.length; i++) {
            if(action.payload[i].id % 2 === 0) { // index is even
                newState.push(action.payload[i]);
            }
        }
        console.log(newState)
        return newState
        
      default:
        return initialState
    }
}
 export default dataReducer;
