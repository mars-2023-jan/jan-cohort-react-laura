import { combineReducers } from "redux";
import colorReducer from "./colorReducer";
import counterReducer from "./counterReducer";
import dataReducer from "./dataReducer"

export const rootReducer = combineReducers({
    count: counterReducer,
    favColor: colorReducer,
    data: dataReducer,
})