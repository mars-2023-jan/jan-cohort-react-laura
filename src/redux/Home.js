import React from 'react';
import { useSelector } from 'react-redux';
import { increment, decrement, getData} from './actions';
import { useDispatch } from 'react-redux';

function Home(props) {
    console.log('State is getting accessed')
    const state = useSelector(state=>state)
    const dispatch = useDispatch()
    return (
        <div>
            <h2>counter: {state.count.counter}</h2>
            <button onClick={() => dispatch(decrement())}>Subtract</button>
            <button onClick={() => dispatch(increment())}>Add</button>
            <hr />
            <input type ='radio' name='color' value='Blue'
            onClick={(e)=>dispatch({type:'CHANGECOLOR', payload: e.target.value})}/>BLUE 
            <input type ='radio' name='color' value='Green'
            onClick={(e)=>dispatch({type:'CHANGECOLOR', payload: e.target.value})}/>GREEN
            <input type ='radio' name='color' value='Red'
            onClick={(e)=>dispatch({type:'CHANGECOLOR', payload: e.target.value})}/>RED
            <p>My favorite color is {state.favColor.color}</p>
        <hr />
        <button onClick={() => dispatch(getData())}>Get Data</button>
        
        {/* <div>
        <table>
            <thead>
                <tr>
                    <th>USER ID</th>
                    <th>ID</th>
                    <th>TITLE</th>
                </tr>
            </thead>
            <tbody>
            {           state.data.length > 0 
                        ?
                        state.data.map((data) => {
                            return(
                                <tr key = {data.id}>
                                    <td>{data.userId}</td>
                                    <td>{data.id}</td>
                                    <td>{data.title}</td>
                                </tr>
                            )
                        })
                        :
                        'No Data Found'
                    }
            </tbody>
        </table>
        </div> */}

        </div>
    );
}

export default Home;