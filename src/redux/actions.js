import axios from "axios"

export const increment = () => {
    return { 
        type: "INCREMENT"
    }
}

export const decrement = () => {
    return {
        type: "DECREMENT"
    }
}

export const getData = (payload) =>{
    return async(dispatch) => {
        console.log("fetching data...")
        const response = await axios.get("http://localhost:8080/api/SortedProducts");
        dispatch({
            type: "FETCH-DATA",
            payload: response.data
        })
    }
}