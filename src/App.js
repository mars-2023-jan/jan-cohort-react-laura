// import logo from './logo.svg';
// import './App.css';
// import Headerdemo from "./components/Header";
// import Subheaderdemo from "./components/Subheader";

// import Head from "./components/Head";
// import Menu from "./components/Menu";
// import Content from "./components/Content";
// import Footer from "./components/Footer";
// import RouteDemo from "./components/RouteDemo"
//import LoginForm from './components/LoginRoutes';
//import EmpDetails from './components/crud/EmpDetails'
//import Reduced from './components/Reducer';

// import counterReducer from './redux/counterReducer';
 import {configureStore} from "@reduxjs/toolkit"
 import { Provider} from 'react-redux'
import myLogger from './redux/myLogger'
 import capAtTen from './redux/capAtTen'
// import colorReducer from './redux/colorReducer';
 import { rootReducer } from './redux/rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
 import thunk from 'redux-thunk';

import CRUDApp from './components/ProductCRUD/CRUDApp';


const reduxDevTools = composeWithDevTools();

// USE THESE FOR DEMO
//import Hobbies from "./components/Hobbies";

//import LoginForm from './components/LoginRoutes';

//import Budget2 from "./components/Budget2";

// import './App.css';
//  import EmpDetails from './components/crud/EmpDetails'

function App() {
const store = configureStore({reducer:rootReducer, middleware:[myLogger, capAtTen, thunk], devTools:reduxDevTools})
  return (
    console.log('Creating store...'),
<Provider store={store} >
       <CRUDApp /> 

</Provider>

  );
}

export default App;