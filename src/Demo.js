import React from 'react';

function Demo(props) {
    return (
        <div>
            <h1>Hello World</h1>
            <p data-testId="testid">This is a testing paragrraph.</p>
            <div>
                <ul>
                    <li>Count</li>
                    <li>Color</li>
                    <li>Testing</li>
                    <li>Development</li>
                </ul>
            </div>
        </div>      
    );
}

export default Demo;