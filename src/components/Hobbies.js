import React, { useEffect, useState } from 'react';


function Hobbies(props) {
    const [enjoys, setEnjoys] = useState("");
    const [preferenceSports, setPreferenceSports] = useState(false);
    const [preferenceTravel, setPreferenceTravel] = useState(false);
    const [preferenceReading, setPreferenceReading] = useState(false);
    const [preferenceMusic, setPreferenceMusic] = useState(false);
    const [preferenceMovies, setPreferenceMovies] = useState(false);

    const togglePreference = hobby => {
        if (hobby.target.name === 'sports') {
            setPreferenceSports(!preferenceSports);  
        }
        if (hobby.target.name === 'travel') {
            setPreferenceTravel(!preferenceTravel);   
        }
        if (hobby.target.name === 'reading') {
            setPreferenceReading(!preferenceReading);  
        }
        if (hobby.target.name === 'music') {
            setPreferenceMusic(!preferenceMusic);  
        }
        if (hobby.target.name === 'movies') {
            setPreferenceMovies(!preferenceMovies);  
        }
    }

    useEffect(() => {
        setEnjoys("");
        if (preferenceSports){
            setEnjoys(enjoys => enjoys + " sports ")
        }
        if (preferenceTravel){
            setEnjoys(enjoys => enjoys + " travel ")
        }
        if (preferenceReading){
            setEnjoys(enjoys => enjoys + " reading ")
        }
        if (preferenceMusic){
            setEnjoys(enjoys => enjoys + " music ")
        }
        if (preferenceMovies){
            setEnjoys(enjoys => enjoys + " movies ")
        }
    });

    return (
        <div>
            <h2>Please check each activity you enjoy!</h2>
            <input type="checkbox" checked={preferenceSports} onChange={togglePreference} name="sports" id="sports" value="sports" />
            <label for="sports">I enjoy sports.</label><br/>   
            <input type="checkbox" checked={preferenceTravel} onChange={togglePreference} name="travel" id="travel" value="travel" />
            <label for="travel">I enjoy travel.</label><br/>
            <input type="checkbox" checked={preferenceMusic} onChange={togglePreference} name="music" id="music" value="music" />
            <label for="music">I enjoy music.</label><br/>
            <input type="checkbox"checked={preferenceReading} onChange={togglePreference} name="reading" id="reading" value="reading" />
            <label for="reading">I enjoy reading.</label><br></br>
            <input type="checkbox" checked={preferenceMovies} onChange={togglePreference} name="movies" id="movies" value="movies" />
            <label for="movies">I enjoy movies.</label><br></br>

            <h2>Your favorite hobbies are: {enjoys}</h2>
        </div>
        
    );
}

export default Hobbies;