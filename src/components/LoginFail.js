import React from 'react';

function LoginFail(props) {
    return (
        <div>
            <h2>Login Failed!</h2>
            <h3>You cannot access the Top Secret Information in my Exclusive Portal.</h3>
        </div>
    );
}

export default LoginFail;