import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";


function LoginForm(props) {
    const navigate = useNavigate()
    const [userInfo, setUserInfo] = useState({
        uname: "",
        uemail: "",
        upassword: "",
    })

    const submitClick = (e) => {
          if ((userInfo.uname === "Laura") && (userInfo.uemail === "laurak@marsreturnship.com") && (userInfo.upassword === "pwd123")) {
            navigate("/loginsuccess");
        } else  {
            navigate("/loginfail")   
        }
    }
    const userChange = (i) => {
        const name = i.target.name;
        const value = i.target.value;
        setUserInfo({
            ...userInfo,
            [name]: value,
         });   
    }

    
    return (
        <div>
            <h2>Super Duper Secret App</h2>
            <h3>Highly Secure Login Form</h3>
                <label for="uname">Name:</label>
                <input type="text" name="uname" id="uname" value={userInfo.name} onChange={userChange} /> <br /><br />
                <label for="uemail">Email:</label>
                <input type="email" name="uemail" id="uemail" value={userInfo.email} onChange={userChange} /> <br /> <br />
                <label for="upassword">Password:</label>
                <input type="password" name="upassword" id="upassword" value={userInfo.password} onChange={userChange}  /> <br /><br />
                <button type="button" name="submit" onClick={submitClick}>Enter at Your Own Risk!</button>
            
        </div>
    );
}
export default LoginForm;