import React from 'react';

function LoginSuccess() {
    
    return (
        <div>
            <h2>Congratulations!</h2>
            <h3>You have been granted Top Security Clearance access to my Super Secret Portal.</h3>
        </div>
    );
    }

export default LoginSuccess;