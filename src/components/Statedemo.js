import React, { Component } from "react";

class Statedemo extends Component{
    constructor(){
        super();
        this.state = {
            color: "Blue",
            count: 0,
        }
        this.updateState = this.updateState.bind(this)
    }
    updateState(){
        this.setState ({
            color: "Green"
        })
    }

    render(){
        return(
            <div>
                <p>{this.state.color}</p>
                <p>{this.state.count}</p>
                <button onClick={this.updateState}>Click to update Color</button>
            </div>
        )            
    }
}
export default Statedemo
