import React from 'react';
// import Statedemo from "./Statedemo";
// import Hooks from "./Hooks";
// import Budget from "./Budget";
// import Budget3 from "./Budget2"
import Hobbies from "./Hobbies";

function Content(props) {
    return (
        <div id="content">
            {/* <Statedemo /> */}
            {/* <Hooks /> */}
            {/* <p><strong>First Budget Using Class... scroll down for second version Using Only Functions</strong></p>
            <Budget />
            <hr/>
            <p><strong>Second Budget Using Only Functions... scroll up for first version Using Class</strong></p>
            <Budget3 /> */}
            <Hobbies />

         </div>
    );
}

export default Content;