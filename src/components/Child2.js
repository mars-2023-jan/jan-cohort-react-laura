import React from 'react';

function Child2({setMessage}) { 
    return (
        <div>
            <button onClick={() => setMessage('message from child 2')}>Child 2</button>
        </div>
    );
}

export default Child2;