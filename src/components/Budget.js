import React, { Component } from 'react';

class Budget extends Component {
    constructor(props){
        super(props);
        this.state={
            total:0,
            mortgage:0,
            bills:0,
            savings:0,
            balance:0
        }
    this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        this.setState({[e.target.name]: e.target.value}, function() {
            this.setState({balance: this.state.total - this.state.mortgage - this.state.bills - this.state.savings});
        });
    }

// function Budget(props){

//     const [inputText, setInputText] = useState("");

//     const initBudget = (e: ChangeEvent<HTMLInputElement>) => {
//       // Store the input value to local state
//       setInputText(e.target.value);
//       };


    render(){
    return (
        <div>
            
            <ul class="wrapper">

                <li><h2>Monthly Budget Calculator Version 1 Using Class</h2></li>

                <li><h3>Income</h3></li>

                <li class="form-row">
                    <label for="total">Total Monthly Budget:</label>
                    <input value={this.state.total} type="number" id="total" name="total" size="5" onChange={this.handleChange} />
                </li>

                <li><h3>Expenses</h3></li>

                <li class="form-row">
                    <label for="mortgage">Mortgage Payment:</label>
                    <input value={this.state.mortgage} type="number" id="mortgage" name="mortgage" size="5"  onChange={this.handleChange}/>
                </li>

                <li class="form-row">
                    <label for="bills">Other Bills:</label>
                    <input value={this.state.bills} type="number" id="bills" name="bills" size="5" onChange={this.handleChange} />
                </li>

                <li class="form-row">
                    <label for="savings">Savings:</label>
                    <input value={this.state.savings} type="number" id="savings" name="savings" size="5"  onChange={this.handleChange}/>
                </li>

                <li><h3>Fun Money!</h3></li>

                <li class="form-row">
                    <label for="left">Remaining Amount:</label>
                    <input value={this.state.balance} type="number" id="balance" name="balance" size="5" />
                </li>

            </ul>

        </div>
    );
    }
}


export default Budget;