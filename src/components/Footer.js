import React from 'react';

function Footer(props) {
    return (
        <div id="footer">
            <ul>
                <li>Address</li>
                <li>Contact Info</li>
                <li>Copyright</li>
            </ul>
        </div>
    );
}

export default Footer;