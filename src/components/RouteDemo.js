import React from 'react';
import { BrowserRouter, Link, Routes, Route } from 'react-router-dom';
import Error from './Error';
import Budget from './Budget';
import Budget2 from './Budget2';
import Hobbies from './Hobbies';
import Parent from './Parent';


function RouteDemo(props) {
    return (
        <div>
            <h1>Route Demo</h1>
            <BrowserRouter>
                <nav>
                    <ul>
                        <li><Link to = "/budget">Budget Classes</Link></li>
                        <li><Link to = "/budget2">Budget Fuction</Link></li>
                        <li><Link to = "/hobbies">Hobbies</Link></li>
                        <li><Link to = "/dynamic">Dynamic</Link></li>
                    </ul>
                    <Routes>
                        <Route path="/budget" element={<Budget />} />
                        <Route path="/budget2" element={<Budget2 />} />
                        <Route path="/hobbies" element={<Hobbies />} />
                        <Route path="/dynamic" element={<Parent />} />
                        <Route path="*" element={<Error />} />
                    </Routes>
                </nav>
            </BrowserRouter>
        </div>
    );
}

export default RouteDemo;