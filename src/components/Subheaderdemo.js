import React from "react";

class Subheaderdemo extends React.Component{
    constructor(){
        super();
        this.state = {
            name: "Sara",
            id: 101
        }
    }
    render() {
        return (
            <div>
            <h3>This is a subheader written as a class component.</h3>
                <p>Scope: {this.props.scope}</p>
                <p>The state of the component is {this.state.name} {this.state.id}</p>
            </div>
        )
    }
}

export default Subheaderdemo;