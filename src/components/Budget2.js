import React, { useState } from 'react';

function Budget2(props) {
    const [state, setState] = useState({
        total: 0,
        mortgage: 0,
        bills: 0,
        savings: 0,
        balance: 0,

    })


    const processChange = evt => {
        const name = evt.target.name;
        const value = evt.target.value;
        setState({
            ...state,
            [name]: value,
         });   
    }

   

    return (
        <div>
            <ul class="wrapper">

                <li><h2>Monthly Budget Calculator Version 2 Using Function</h2></li>

                <li><h3>Income</h3></li>

                <li class="form-row">
                    <label for="total">Total Monthly Budget:</label>
                    <input value={state.total} type="number" id="total" name="total" size="5" onChange={processChange}  />
                </li>

                <li><h3>Expenses</h3></li>

                <li class="form-row">
                    <label for="mortgage">Mortgage Payment:</label>
                    <input value={state.mortgage} type="number" id="mortgage" name="mortgage" size="5" onChange={processChange}  />
                </li>

                <li class="form-row">
                    <label for="bills">Other Bills:</label>
                    <input value={state.bills} type="number" id="bills" name="bills" size="5"  onChange={processChange}  />
                </li>

                <li class="form-row">
                    <label for="savings">Savings:</label>
                    <input value={state.savings} type="number" id="savings" name="savings" size="5"  onChange={processChange} />
                </li>

                <li><h3>Fun Money!</h3></li>

                <li class="form-row">
                    <label for="balance">Remaining Amount:</label>
                    <input value={(state.total) - (state.mortgage) - (state.bills) - (state.savings)}type="number" id="balance" name="balance" size="5" />
                </li>
            </ul>
        </div>
    );
}

export default Budget2;