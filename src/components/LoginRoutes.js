import React from 'react';
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import LoginForm from './LoginForm';
import LoginSuccess from './LoginSuccess';
import LoginFail from './LoginFail';
import Error from './Error';

function LoginRoutes(props) {
    return (
        <div>
            <BrowserRouter>
                    <Routes>
                        <Route path="/loginform" element={<LoginForm />} />
                        <Route path="/loginsuccess" element={<LoginSuccess />} />
                        <Route path="/loginfail" element={<LoginFail />} />
                        <Route path="*" element={<Error />} />
                    </Routes>
                    <ul>
                        <li><Link to = "/loginform">Return To Login Form</Link></li>
                    </ul>
            </BrowserRouter>
        </div>
    );
}


export default LoginRoutes;