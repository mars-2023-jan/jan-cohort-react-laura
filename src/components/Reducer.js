import React, {useState} from 'react';
import '/node_modules/bootstrap/dist/css/bootstrap.min.css'

function Reducer (props) {
    const[inputVal, setInputVal] = useState('')
    const[counter, setCounter] = useState(0)
    const[color, setColor]= useState(false)
    
    return(
        <div className='form-group' style={{color:color?'red':'black'}}>
            <input className ='form-control'
            type= 'text'
            placeholder = 'Enter Text'
            onChange ={e => setInputVal = (e.target.value)} />
            <br/><br/>
            <p>{inputVal}</p>
            <p>{counter}</p>
            <button
                type='button'
                className= 'btn btn-info'
                onClick ={(e) => setCounter(counter+1)}>+</button>
            <button
                type='button'
                className= 'btn btn-info'
                onClick ={(e)=> setCounter(counter-1)}>-</button>
            <button
                type='button'
                className= 'btn btn-dark'
                onClick ={()=> setColor(!color)}>Toggle Color</button>

        </div>
    );
}

export default Reducer;