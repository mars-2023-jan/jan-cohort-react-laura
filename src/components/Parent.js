import React from 'react';
import { useNavigate } from 'react-router-dom';

function Parent(props) {
    const navigate = useNavigate();
    function handleClick(){
        navigate("/");
        // to go back a page instead of to home
        // navigate(-1);
        // to go next page instead of to home
        // navigate(1);
        // to go specific page instead of to default home
        // navigate("/expense");
    }
    return (
        <div>
            <button onClick={handleClick}>Go Home</button>
        </div>
    );
}

export default Parent;