import React, {useEffect, useState} from 'react';
import Employees from './Employees';
import { useNavigate } from 'react-router-dom';

function AddEmployee(props) {
    const[name, setName] = useState('')
    const[age, setAge] = useState('')
    const[id, setId] = useState('')
    let navigate = useNavigate()

    const handleClick = (e) =>{
        console.log('Adding...... ')
        const newemployee = {"id": id, "name": name, "age": age}
        Employees.push(newemployee)
        navigate('/')
    }

    useEffect(() => {
        setId(localStorage.getItem('nextid'))
    }, [])


    return (
        <div>
            <h2>Add Employee</h2>
            <form>
                <table>
                    <tr>
                        <td><label for='id'>ID: </label></td>  
                        <td class="align-left">
                            <input
                            class='no-edit'
                            id='id'
                            type='text'
                            size='20'
                            value={id}
                            readOnly /> 
                        </td>
                    </tr>
                    <tr>
                        <td><label for="name">Name: </label></td>
                        <td>
                        <input
                        id="name"
                        type='text'
                        size='20'
                        placeholder=''
                        value={name}
                        onChange={(e)=>setName(e.target.value)} />
                        </td>
                    </tr>
                    <tr>
                        <td> <label for='age'>Age: </label></td>
                        <td>
                        <input
                        id='age'
                        type='number'
                        size='20'
                        placeholder=''
                        value={age}
                        onChange={(e)=>setAge(e.target.value)} />
                        </td>
                    </tr>   
                    </table>
                <br />
                <button onClick={(e) => handleClick(e)} type ='submit'>Add Employee</button>
            </form>
        </div>
    );
}

export default AddEmployee;