import React, {useEffect, useState} from 'react';
import Employees from './Employees';
import { useNavigate } from 'react-router-dom';

function EditEmployee(props) {
    const[name, setName] = useState('')
    const[age, setAge] = useState('')
    const[id, setId] = useState('')
    let index = Employees.findIndex(val => val.id == id)
    let navigate = useNavigate()

    const handleClick = (e) =>{
        console.log('Editing... ')
        e.preventDefault();
        let emp = Employees[index];
        emp.name = name;
        emp.age = age;
        navigate('/')
    }

    useEffect(() => {
        setName(localStorage.getItem('name'))
        setAge(localStorage.getItem('age'))
        setId(localStorage.getItem('id'))
    }, [])

    return (
        <div>
            <h2>Edit Employee</h2>
            <form>
                <table>
                    <tr>
                        <td><label for='id'>ID: </label></td>
                        <td class='align-left'>
                        <input
                        class='no-edit'
                        id='id'
                        type='text'
                        size='20'
                        value={id}
                        readOnly />
                        </td>
                    </tr>
                    <tr>
                        <td><label for='name'>Name: </label></td>
                        <td>
                        <input
                        id='name'
                        type='text'
                        placeholder='Enter Name'
                        size='20'
                        value={name}
                        onChange={(e)=>setName(e.target.value)} />
                        </td>
                    </tr>
                    <tr>
                        <td><label for='age"'>Age: </label></td>
                        <td>
                        <input
                        id='age'
                        type='number'
                        placeholder='Enter Age'
                        size='3'
                        value={age}
                        onChange={(e)=>setAge(e.target.value)} />
                        </td>
                    </tr>
                </table>
                <br />
                <button onClick={(e) => handleClick(e)} type ='submit'>Save Update</button>
            </form>
        </div>
    );
}

export default EditEmployee;