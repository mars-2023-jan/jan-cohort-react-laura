import React from 'react';
import Employees from './Employees';
import './../crud/emp.css'
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getData} from '../ProductCRUD/actions';
import { useDispatch } from 'react-redux';

function EmpDetails(props) {
    let navigate = useNavigate()

    const handleDelete = (id) => {
        let index = Employees.map(e => e.id).indexOf(id);
        Employees.splice(index, 1);
        navigate('/')    

    }
    const handleEdit = ((id, name, age) => {
        localStorage.setItem('id', id)
        localStorage.setItem('name', name)
        localStorage.setItem('age', age)
    })

    const handleAdd = (i) => {
        if (Employees.length > 0) {
            let nextIndex = (Math.max(...Employees.map(i => i.id)) + 1)
            localStorage.setItem('nextid', nextIndex)
        } else {
            localStorage.setItem('nextid', 1)
        }
        
        }

    return (
        <div>
            <h2>Employee Data</h2>
            <table>
                <thead>
                    <tr>
                        <th class="gray-background">ID</th>
                        <th class="gray-background">NAME</th>
                        <th class="gray-background">AGE</th>
                        <th class="gray-background" colSpan={2}>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        Employees.length > 0 /* check if there are emloyees */
                        ?
                        Employees.map((emp) => {
                            return(
                                <tr key = {emp.id}>
                                    <td>{emp.id}</td>
                                    <td>{emp.name}</td>
                                    <td>{emp.age}</td>
                                    <td>
                                        <Link to = '/edit'>   
                                        <button onClick={() => handleEdit(emp.id, emp.name, emp.age)}>Edit</button>
                                        </Link> 
                                    </td>
                                    <td>
                                        <button onClick={() => handleDelete(emp.id)}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                        :
                        'No Data Found'
                    }
                </tbody>
            </table>
            <br />
            <Link to = '/add'>   
            <button onClick={() => handleAdd()}>Add New</button>
            </Link> 
        </div>
    );
}

export default EmpDetails;