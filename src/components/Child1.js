import React from 'react';

function Child1({message}) { //destructuring props and using only the message
    return (
        <div>
            <p>{message}</p>
        </div>
    );
}

export default Child1;