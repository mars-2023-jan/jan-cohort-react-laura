import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './CRUD.css';

const CRUDApp = () => {
   const [id, setId] = useState('');
   const [name, setName] = useState('');
   const [desc, setDesc] = useState('');
   const [price, setPrice] = useState('');
   const [products, setProducts] = useState([]);

   useEffect(() => {
      fetch("http://3.86.155.150:8181/api/SortedProducts")
         .then((response) => response.json())
         .then((data) => {
            console.log(data);
            setProducts(data);
         })
         .catch((err) => {
            console.log(err.message);
         });
   }, []);

   const addProduct = async (id, name, desc, price) => {
      await fetch("http://localhost:8080/api/product", {
         method: 'POST',
         body: JSON.stringify({
            prod_id: id,
            prod_name: name,
            prod_desc: desc,
            price: price,
         }),
         headers: {
            'Content-type': 'application/json; charset=UTF-8',
         },
      })
         .then((response) => response.json())
         .then((data) => {
            setProducts((products) => [data, ...products]);
            setId('');
            setName('');
            setDesc('');
            setPrice('');
         })
         .catch((err) => {
            console.log(err.message);
         });
   };
   
   const handleSubmit = (e) => {
      e.preventDefault();
      addProduct(id, name, desc, price);
   };    


   // const client = axios.create({
   //    baseURL: "http://localhost:8080/product" 
   // });

   // const deleteProduct = (id) => {
   //    var url = ("http://localhost:8080/product/" + id);
   //    client.delete(`${id}`);
   //    setProducts(
   //       products.filter((product) => {
   //          return product.prod_id !== id;
   //       })
   //    );
   // };

   const deleteProduct = async (id) => {
      await fetch(`http://localhost:8080/api/product/${id}`, {
         method: "PUT",
         credentials: "include",
         headers: {
            'Content-type': 'application/json',
         },
      }).then((response) => {
         if (response.status === 200) {
            setProducts(
               products.filter((product) => {
                  return product.id !== id;
               })
            );
         } else {
            return;
         }
      });
      };


   const onChangeInput = (e, prod_id) => {
         const { name, value} = e.target
       
         const editData = products.map((item) =>
           item.prod_id === prod_id && name ? { ...item, [name]: value } : item
         )
       
         console.log('editData', editData)
       
         setProducts(editData)
       }

   const editProduct = async (id, name, desc, price) => {
      var url = ("http://localhost:8080/api/product/" + id);
      await fetch(url, {
         method: 'PUT',
           body: JSON.stringify({
            prod_id: id,
            prod_name: name,
            prod_desc: desc,
            price: price,
         }),
         headers: {
            'Content-type': 'application/JSON',
         },
      })
   };


   return (
    <div>
      <table>
      <tr>
         <th>Product ID</th>
         <th>Product Name</th>
         <th>Product Description</th>
         <th>Product Price</th>
         <th colspan="2" >Action</th>
      </tr>
       {products.map((product) => {
          return (
             <tr key={product.prod_id}>
                <td><input name= "prod_id" value = {product.prod_id} type= "text" onChange={(e) => onChangeInput(e, product.prod_id)}></input></td>
                <td><input name= "prod_name" value = {product.prod_name} type= "text" onChange={(e) => onChangeInput(e, product.prod_id)}></input></td>
                <td><input name= "prod_desc" value = {product.prod_desc} type= "text" onChange={(e) => onChangeInput(e, product.prod_id)}></input></td>
                <td><input name= "price" value = {product.price} type= "text" onChange={(e) => onChangeInput(e, product.prod_id)}></input></td>
                <td><button onClick={() => editProduct(product.prod_id, product.prod_name, product.prod_desc, product.price)}>Save Changes</button></td>
                <td><button onClick={() => deleteProduct(product.prod_id)}>Delete</button></td>
             </tr> 
          );
       })}
       </table>
   <br />
    <form onSubmit={handleSubmit}>
      <label>Product ID: </label>
       <input type="text" className="form-control" value={id}
          onChange={(e) => setId(e.target.value)}
       /><br />
       <label>Product Name: </label>
       <input type="text" className="form-control" value={name}
          onChange={(e) => setName(e.target.value)}
       /><br />
       <label>Product Description:</label>
       <input type="text" className="form-control" value={desc}
          onChange={(e) => setDesc(e.target.value)}
       /><br />
       <label>Price</label>
       <input type="text" className="form-control" value={price}
          onChange={(e) => setPrice(e.target.value)}
       /><br />
       <button type="submit">Add Product</button>
    </form>

</div>
    );
};

export default CRUDApp;