import {useState, useEffect} from 'react';

function Hooks(){
    const [color, setColor] = useState("Brown")
    const [count, setCount] = useState(0)
    const [calculation, setCalculation] = useState(0)

    const [car, setCar] = useState({
            make: 'Ford',
            model: 'Mustang',
            year: 1990,
            color: 'red'
    })

     return  (  

        <div>

               <h2>My favorite color is {color}</h2>

            <button type="button" onClick={() => setColor('White')}>Set Color</button>
            <h3>my {car.make} {car.model} car built in {car.year} is of color {car.color}</h3>
            
            {/* using spread to maintain the rest of the values */}
            <button type="button" onClick={() => setCar(oldCar => {
                return {...oldCar, color: 'blue'}})}>Change Color</button>

            <h2>Page loaded times</h2>
            
        </div>
    )}
export default Hooks;

