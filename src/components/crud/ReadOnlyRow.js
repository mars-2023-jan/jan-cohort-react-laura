import React from 'react'
import {FaEdit} from 'react-icons/fa';
import {RiDeleteBinLine} from 'react-icons/ri'


const ReadOnlyRow = ({emp, handleEditClick, handleDeleteClick}) => {
    return (
        <div>
            <table>
            <tr key = {emp.id}>
                <td>{emp.name}</td>
                <td>{emp.age}</td>
                <td>
                    <button type="button"
                    onClick={(e)=> handleEditClick(e, emp)}>
                        <FaEdit />
                    </button>
                </td>
                <td>
                    <button type="button"
                    onClick={(e)=> handleDeleteClick(e, emp)}>
                        <RiDeleteBinLine />
                    </button>
                </td>
            </tr>
        </table>
        </div>
    )
}

export default ReadOnlyRow
