import React from 'react'
import {FaSave} from 'react-icons/fa'

const EditableRow = ({editEmpData, handleEditChange}) => {
    return (
        <div>
            <table>
            <tr>
                <td>
                    <input
                        type = 'text'
                        required = 'required'
                        name = 'name'
                        value = {editEmpData.name}
                        placeholder = 'Enter Name'
                        onChange={handleEditChange}
                    />
                </td>
                <td>
                    <input
                        type = 'number'
                        required = 'required'
                        name = 'age'
                        value = {editEmpData.age}
                        placeholder = 'Enter Age'
                        onChange={handleEditChange}
                    />
                </td>
                <td colspan="2">
                    <button type="submit">
                        <FaSave />
                    </button>
                </td>
            </tr>
        </table>
        </div>
    )
}

export default EditableRow
