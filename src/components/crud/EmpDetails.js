import React, {Fragment, useState }from 'react';
import Employees from './Employees';
import './../crud/emp.css'
import { Link } from 'react-router-dom';
import {nanoid} from 'nanoid';
import { useNavigate } from 'react-router-dom';
import ReadOnlyRow from './ReadOnlyRow';
import EditableRow from './EditableRow';

function EmpDetails(props) {

    const [employees, setEmployees] = useState(Employees)

    const [editEmpId, setEditEmpId] = useState(null)

    const [addFormData, setAddFormData] = useState({
        name: '',
        age: '',
    })

    const[editEmpData, setEditEmpData] = useState({
        name: '',
        age: '',
    })

    const handleAddFormChange= (e) => {
        e.preventDefault();

        const fieldName = e.target.getAttribute('name');
        const fieldValue = e.target.value;
        
        const newFormData = {...addFormData};
        newFormData[fieldName] = fieldValue;
        setAddFormData(newFormData);
     
    }

    const handleEditChange = (e) => {
        e.preventDefault();
        const fieldName = e.target.getAttribute('name');
        const fieldValue = e.target.value;

        const newFormData = {...editEmpData};
        newFormData[fieldName] = fieldValue;
        setEditEmpData(newFormData);
    }

    const handleAddFormSubmit = (e) => {
        e.preventDefault();
        const newEmp = {
            id:nanoid(),
            name:addFormData.name,
            age:addFormData.age
        }

        const newEmployees = [...employees, newEmp]
          setEmployees(newEmployees);
   

    }

    const  handleEditClick = (e, emp) => {
        e.preventDefault();
        setEditEmpId(emp.id)

        const formValues = {
            name: emp.name,
            age: emp.age,
        }
        setEditEmpData(formValues)
    }

    const  handleDeleteClick = (e, emp) => {
        e.preventDefault();
        const delEmployees = [...employees]
        const id = emp.id
        const index = delEmployees.findIndex((emp)=>emp.id === id)
        delEmployees.splice(index, 1);
        setEmployees(delEmployees);
    }

    const handleEditEmpSubmit = (e)=> {
        e.preventDefault();

        const editedEmp = {
            id: editEmpId, 
            name: editEmpData.name,
            age: editEmpData.age
        }
        const newEmp = [...employees]
        const index = employees.findIndex((emp)=>emp.id === editEmpId)
        newEmp[index]= editedEmp;
        setEmployees(newEmp);
        setEditEmpId(null)

    }
    // const handleDelete = (id) => {
    //     let index = Employees.map(e => e.id).indexOf(id);
    //     Employees.splice(index, 1);
    //     navigate('/')    

    // }
    // const handleEdit = ((id, name, age) => {
    //     localStorage.setItem('id', id)
    //     localStorage.setItem('name', name)
    //     localStorage.setItem('age', age)
    // })

    // const handleAdd = (i) => {
    //     if (Employees.length > 0) {
    //         let nextIndex = (Math.max(...Employees.map(i => i.id)) + 1)
    //         localStorage.setItem('nextid', nextIndex)
    //     } else {
    //         localStorage.setItem('nextid', 1)
    //     }
        
    //     }

    return (
        <div>
            <h2>Employee Data</h2>
            <form onSubmit={handleEditEmpSubmit}> 
            <table>
                    <tr>
                        <th class="gray-background">NAME</th>
                        <th class="gray-background">AGE</th>
                        <th class="gray-background" colSpan={2}>ACTION</th>
                    </tr>
             </table>      
                    {
                        employees.length > 0 /* check if there are emloyees */
                        ?
                        employees.map((emp) => {
                            return(
                                <Fragment>
                                    { editEmpId === emp.id ? 
                                    (
                                    <EditableRow  
                                    editEmpData={editEmpData}
                                    handleEditChange ={handleEditChange}
                                    />
                                    )
                                    :
                                    (
                                    <ReadOnlyRow 
                                    emp = {emp} 
                                    handleEditClick = {handleEditClick}
                                    handleDeleteClick = {handleDeleteClick} 
                                    />
                                    )}
                                </Fragment>

                            )
                        })
                                               :
                        'No Data Found'
                    }
            </form>
            <br />
          
            <h2>Add Employee</h2>
            <form onSubmit={handleAddFormSubmit}>
                <table>
                <tr>
                    <td>
                        <input
                        type='text'
                        name= 'name'
                        required= 'required'
                        placeholder='Enter Name'
                        onChange={handleAddFormChange}
                      />
                    </td>
                    <td>
                        <input
                        type='number'
                        name= 'age'
                        required= 'required'
                        placeholder='Enter Age'
                        onChange={handleAddFormChange}
                     />
                     </td>
                     <td>
                     <button type='submit'>Add</button>
                     </td>
                </tr>
                </table>
            </form>
        
        </div>
    );
}

export default EmpDetails;