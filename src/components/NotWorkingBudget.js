import React, { useState } from 'react';

function Budget2(props) {
    const [state, setState] = useState({
        total: 0,
        mortgage: 0,
        bills: 0,
        savings: 0,
        balance: 0,

        storedTotal: 0,
        smortgage: 0,
        sbills: 0,
        ssavings: 0,
        storedBalance: 0,
        preserveBalance: 0,
        preserveExpense: 0,
 
    })

    const storeTotal = evt => {
        const balance = evt.target.value;
        setState({
            ...state,
            storedTotal: balance,
        });   
    }

    const preserveValues = evt => {
        setState({
            ...state,
            preserveBalance: state.balance,
            preserveExpense: state.expense,
        });   
    }

    const changeTotal = evt => {
        const name = evt.target.name;
        const value = evt.target.value;
        setState({
            ...state,
            [name]: value,
            balance: (value - state.smortgage - state.sbills - state.ssavings),
        });   
    }

    const storeExpense = evt => {
        const name = evt.target.name;
        const value = evt.target.value;
        const addPrefix = "s" + name;
      
        setState({
            ...state,
            [addPrefix]: value,
            storedBalance: (state.storedTotal - state.smortgage - state.sbills - state.ssavings)
        });   
    }

    const changeExpense = evt => {
        const name = evt.target.name;
        const value = evt.target.value;
        const tempBal = 0;
        const tempExp = 0;
        alert(state.preserveBalance)
        setState({
            ...state,
            [name]: value,
            tempBal: (state.preserveBalance + state.preserveExpense),
            tempExp: Number(value + value),
            balance: (tempBal - tempExp),
         });   
    }

    // const changeExpense = evt => {
    //     const name = evt.target.name;
    //     const value = evt.target.value;
    //     setState({
    //         ...state,
    //         [name]: value,
    //         balance: (state.preserveValue - value),
    //      });   
    // }

    // const keyDeleteOrBackspace = evt => {
    //     const name = evt.target.name;
    //     const value = evt.target.value;
    //     if ((evt.key === "Delete") || (evt.key === "Bakcspace")) {
    //         alert("Pressed Delete or Backspace... now what?")
    //     }
    // }

    return (
        <div>
            <ul class="wrapper">

                <li><h2>Monthly Budget Calculator</h2></li>

                <li><h3>Income</h3></li>

                <li class="form-row">
                    <label for="total">Total Monthly Budget:</label>
                    <input value={state.total} type="number" id="total" name="total" size="5" onChange={changeTotal} onBlur={storeTotal} />
                </li>

                <li><h3>Expenses</h3></li>

                <li class="form-row">
                    <label for="mortgage">Mortgage Payment:</label>
                    <input value={state.mortgage} type="number" id="mortgage" name="mortgage" size="5" onChange={changeExpense} onBlur={storeExpense} onFocus={preserveValues} />
                </li>

                <li class="form-row">
                    <label for="bills">Other Bills:</label>
                    <input value={state.bills} type="number" id="bills" name="bills" size="5"  onChange={changeExpense} onBlur={storeExpense} onFocus={preserveValues} />
                </li>

                <li class="form-row">
                    <label for="savings">Savings:</label>
                    <input value={state.savings} type="number" id="savings" name="savings" size="5"  onChange={changeExpense} onBlur={storeExpense} onFocus={preserveValues} />
                </li>

                <li><h3>Fun Money!</h3></li>

                <li class="form-row">
                    <label for="balance">Remaining Amount:</label>
                    <input value={state.balance} type="number" id="balance" name="balance" size="5" />
                </li>
            </ul>
        </div>
    );
}

export default Budget2;