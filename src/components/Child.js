import React from 'react';

function Child({childToMain}) {
    const data = "the data passed from child to main"
    return (
        <div>
            <button onClick = {() => childToMain(data)}>Click(Child)</button>
        </div>
    );
}

export default Child;